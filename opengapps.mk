DEVICE_PACKAGE_OVERLAYS += \
    vendor/opengapps/overlay/pico

### core
# SetupWizard GooglePartnerSetup Turbo
PRODUCT_PACKAGES += \
    GoogleBackupTransport \
    GoogleContactsSyncAdapter \
    GoogleFeedback \
    GoogleOneTimeInitializer \
    PrebuiltGmsCore \
    GoogleServicesFramework\
    GoogleLoginService\
    GoogleDeviceReg \
    Phonesky \
    GoogleCalendarSyncAdapter \
    GoogleExtShared \
    GoogleExtServices

### etc
PRODUCT_PACKAGES += \
    com.google.widevine.software.drm.xml \
    com.google.android.maps \
    com.google.android.media.effects \
    google.xml \
    google_build.xml \
    google_exclusives_enable.xml \
    default-permissions.xml \
    opengapps-permissions.xml \
    preferred-google.xml

### framework
PRODUCT_PACKAGES += \
    com.google.android.media.effects \
    com.google.android.maps \
    com.google.widevine.software.drm
